﻿using System.ComponentModel.DataAnnotations;

namespace Ajax.ViewModels
{
    public class FriendViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string LastName { get; set; }
        [EmailAddress]
        public string Email { get; set; }
    }
}