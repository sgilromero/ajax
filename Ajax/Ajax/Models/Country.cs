﻿using System.Collections.Generic;
using System.Linq;

namespace Ajax.Models
{
    public class Country
    {
        public int Id { get; set; }

        public int ContinentId { get; set; }

        public string Name { get; set; }        
    }

    public class GeoServices
    {
        static List<Country> _countries = new List<Country>()
        {
            new Country() {ContinentId = 1, Id = 1, Name = "France"},
            new Country() {ContinentId = 1, Id = 2, Name = "Spain"},
            new Country() {ContinentId = 1, Id = 3, Name = "Portugal"},
            new Country() {ContinentId = 1, Id = 4, Name = "Italy"},
            new Country() {ContinentId = 1, Id = 5, Name = "Denmark"},
            new Country() {ContinentId = 1, Id = 6, Name = "Greece"},
            new Country() {ContinentId = 2, Id = 7, Name = "China"},
            new Country() {ContinentId = 2, Id = 8, Name = "India"},
            new Country() {ContinentId = 2, Id = 9, Name = "Japan"},
            new Country() {ContinentId = 2, Id = 10, Name = "Korea"},
            new Country() {ContinentId = 2, Id = 11, Name = "Taiwan"},
            new Country() {ContinentId = 3, Id = 12, Name = "United States"},
            new Country() {ContinentId = 3, Id = 13, Name = "Brazil"},
            new Country() {ContinentId = 3, Id = 14, Name = "Mexico"},
            new Country() {ContinentId = 3, Id = 15, Name = "Colombia"},
            new Country() {ContinentId = 3, Id = 16, Name = "Argentina"},
        };

        public IEnumerable<Country> GetCountriesByContinent(int continentId)
        {
            return _countries.Where(c => c.ContinentId == continentId);
        }
    }
}