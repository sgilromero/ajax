﻿using Ajax.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using static Ajax.Models.Country;

namespace Ajax.Controllers
{
    public class GeoController : Controller
    {
        //
        // GET: /Geo/GetCountries
        [HttpPost]
        public ActionResult GetCountries(int regionId)
        {
            IEnumerable<Country> countries =
                   new GeoServices().GetCountriesByContinent(regionId);
            return Json(countries);
        }
    }
}