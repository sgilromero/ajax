﻿using Ajax.ViewModels;
using System.Web.Mvc;

namespace Ajax.Controllers
{
    public class FriendsController : Controller
    {
        [HttpPost]
        public ActionResult SaveData(FriendViewModel vm)
        {
            return Json(ModelState.IsValid);
        }
    }
}