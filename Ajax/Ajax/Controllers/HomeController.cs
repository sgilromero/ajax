﻿using Ajax.ViewModels;
using System.Web.Mvc;

namespace Ajax.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetPartialView(string name)
        {
            return PartialView("_Greetings", name);
        }

        [HttpPost]
        public ActionResult SaveData(FriendViewModel vm)
        {
            return Json(ModelState.IsValid);
        }
    }
}